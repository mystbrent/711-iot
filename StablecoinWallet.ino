#include <SPI.h>

/* Start of Code */

/* Simple RFID Arduino Sketch(RC522)

The MFRC522 Library used, was created by LJOS here: https://github.com/ljos/MFRC522 
The FastLED Library used, was created by focalintent here: https://github.com/FastLED/FastLED/releases

*/
#include <SoftwareSerial.h>
#include <MFRC522.h> // Include of the RC522 Library
//#include "FastLED.h" // Include of the FastLED library
#include <SPI.h> // Used for communication via SPI with the Module

// WS2812 RGB Stick connection
//#define led_pin 5 // Pin 5 connected to DIN of RGB Stick
#define NUM_LEDS 8 // # of WS2812 LEDs on stick
//CRGB leds[NUM_LEDS]; // FastLED Library Init

#define SDAPIN 10 // RFID Module SDA Pin is connected to the UNO 10 Pin
#define RESETPIN 8 // RFID Module RST Pin is connected to the UNO 8 Pin

#define Buzzer 4 // Pin 3 connected to + pin of the Buzzer

byte FoundTag; // Variable used to check if Tag was found
byte ReadTag; // Variable used to store anti-collision value to read Tag information
//byte TagData[MAX_LEN]; // Variable used to store Full Tag Data
byte TagData[99];
byte TagSerialNumber[5]; // Variable used to store only Tag Serial Number
byte GoodTagSerialNumber[5] = {0xB2, 0xAA, 0x3, 0xA3}; // The Tag Serial number we are looking for

static char respBuffer[4096];

// global constant
const char ssid[] = "Brent";
const char ssidPass[] = "indimapaktan34";
const char hostName[] = "http://172.20.10.10";
const short hostPort = 3000;

unsigned long lastTimeMillis = 0;

boolean isSetup = false;
String response = "";
String soil = "";
boolean sending = false;
boolean opened = false;
char temp[200];

SoftwareSerial esp8266(2, 3); /* RX:D3, TX:D2 */

MFRC522 nfc(SDAPIN, RESETPIN); // Init of the library using the UNO pins declared above

void setup() {
pinMode(Buzzer, OUTPUT); // Set buzzer pin to an Output pin
digitalWrite(Buzzer, LOW); // Buzzer Off at startup
SPI.begin();
esp8266.begin(115200);
Serial.begin(115200);

// Start to find an RFID Module
Serial.println("Looking for RFID Reader");
nfc.begin();
byte version = nfc.getFirmwareVersion(); // Variable to store Firmware version of the Module

// If can't find an RFID Module 
if (! version) { 
Serial.print("Didn't find RC522 board.");
while(1); //Wait until a RFID Module is found
}

// If found, print the information about the RFID Module
Serial.print("Found chip RC522 ");
Serial.print("Firmware version: 0x");
Serial.println(version, HEX);
Serial.println();
reset();
delay(500);
setupConfiguration();
delay(1000);
connect();
delay(500);
establishTCPConnection();
delay(500);
}

void reset() {
  esp8266.println("AT+RST");
  delay(1000);
  if (esp8266.find("OK")) Serial.println("Module Had been reset");
}

void establishTCPConnection() {
  const char cmd[] = "AT+CIPSTART=\"TCP\",\"172.20.10.10\",3000";
  esp8266.println(cmd);
  if (esp8266.find("OK")) {
    Serial.println("TCP Connection Ready.");
    delay(1000);
  }
}

void setupConfiguration() {
  esp8266.println("AT+CWMODE=3");
  delay(500);
  esp8266.println("AT+CIPMUX=1");
  delay(500);
  esp8266.println("AT+CIPSERVER=1,80");
  delay(5000);
}


void connect() {
  const char cmd[] = "AT+CWJAP=\"Brent\",\"indimapaktan34\"";
  esp8266.println(cmd);
  delay(4000);
  if (esp8266.find("OK")) {
    Serial.println("Connected to WIFI.");
  }
}

//void send() {
void send(String value) {
  Serial.println("SENDING...");
  if (millis() - lastTimeMillis > 10000) {
    lastTimeMillis = millis();

    esp8266.println("AT+CIPMUX=1");
    delay(500);
    esp8266.println("AT+CIPSTART=4,\"TCP\",\"172.20.10.10\",3000");
    delay(500);
//    String cmd = getPostRequest("soil");
    String cmd = getPostRequest(value);
    esp8266.println("AT+CIPSEND=4," + String(cmd.length() + 4));
    delay(1000);
    esp8266.println(cmd);
    delay(500);
    esp8266.println("");
  }

}

void loop() {

//delay(500);
// Check to see if a Tag was detected
// If yes, then the variable FoundTag will contain "MI_OK"
FoundTag = nfc.requestTag(MF1_REQIDL, TagData);
//Serial.println(FoundTag);
//if (FoundTag == 2) {
if (FoundTag == MI_OK) {
delay(200);
// Get anti-collision value to properly read information from the Tag
ReadTag = nfc.antiCollision(TagData);
memcpy(TagSerialNumber, TagData, 4); // Write the Tag information in the TagSerialNumber variable

Serial.println("Tag detected.");
Serial.print("Serial Number: ");
for (int i = 0; i < 4; i++) { // Loop to print serial number to serial monitor
Serial.print(TagSerialNumber[i], HEX);
Serial.print(", ");
}
Serial.println("");
Serial.println();


// Check if detected Tag has the right Serial number we are looking for 
for(int i=0; i < 4; i++){
if (GoodTagSerialNumber[i] != TagSerialNumber[i]) {
break; // if not equal, then break out of the "for" loop
}
}
if (true) {
//if (GoodTag == "TRUE"){
Serial.println("Success!!!!!!!");
Serial.println();
//for (int y = 0; y < 3; y++){
//digitalWrite (Buzzer, HIGH) ;// Buzzer On
//delay (50) ;// Delay 1ms 
//digitalWrite (Buzzer, LOW) ;// Buzzer Off
//delay (50) ;// delay 1ms
//}
delay(1500);
send("B2-AA-3-A3");
//send();
//send(TagSerialNumber.toString());
checkResponseValue();
}
else {
Serial.println("TAG NOT ACCEPTED...... :(");
Serial.println();
for (int y = 0; y < 3; y++){
digitalWrite (Buzzer, HIGH) ;// Buzzer On
delay (300) ;// Delay 1ms 
digitalWrite (Buzzer, LOW) ;// Buzzer Off
delay (400) ;// delay 1ms
}
delay(500); 
}
}
}

String getPostRequest(String postData) {
    String post = "POST /";
      post += "accounts";
    post += " HTTP/1.1\r\n";
    post += "Host: ";
    post += hostName;
    post += "\r\n";
    post += "Accept: *";
    post += "/";
    post += "*\r\n";
    post += "Content-Length: ";
//    post += data.length();
    post += postData.length();
    post += "\r\n";
    post += "Content-Type: application/x-www-form-urlencoded\r\n";
    post += "\r\n";
    post += postData;
    
    return post;
}

void printResponse() {
  while (esp8266.available()) {
    Serial.println(esp8266.readStringUntil('\r'));
  }
}

void checkResponseValue() {
  
  if (esp8266.available() > 0) {

    Serial.print("PRINTING RESPONSE:");
    printResponse();
  }
}

/* End of Code */